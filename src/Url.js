import Login from "./pages/auth/Login";
import Register from "./pages/auth/Register";
import Dashboard from "./pages/Dashboard";

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function Url() {
    return (
        <Router>
            <div>
            <Switch>
                <Route path="/" exact component={Login} />
                <Route path="/register" exact component={Register} />
                <Route path="/dashboard" exact component={Dashboard} />
            </Switch>
            </div>
      </Router>
    );
}

export default Url;