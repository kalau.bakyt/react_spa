import Url from "./Url";
import Header from "./pages/layouts/Header";

export default function App() {
  return (
    <div>
      <Header/>
      <Url/>
    </div>
  );
}
